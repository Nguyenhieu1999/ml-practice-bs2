import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from sklearn.linear_model import LinearRegression
from sklearn.metrics import r2_score

data = pd.read_csv("Demo.csv")

plt.figure(figsize=(16, 8))
plt.scatter(
    data['Age'],
    data['PhysicalPoint'],
    c='red'
)
plt.xlabel("Tuổi")
plt.ylabel("Điểm thể lực")
plt.show()

X = data['Age'].values.reshape(-1,1)
y = data['PhysicalPoint'].values.reshape(-1,1)
reg = LinearRegression()
reg.fit(X, y)

predictions = reg.predict(X)
plt.figure(figsize=(16, 8))
plt.scatter(
    data['Age'],
    data['PhysicalPoint'],
    c='Red'
)
plt.plot(
    data['Age'],
    predictions,
    c='blue',
    linewidth=2
)
plt.xlabel("Tuổi")
plt.ylabel("Điểm thể lực")
plt.show()
