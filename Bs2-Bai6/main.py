import pandas as pd
import numpy as np
import math
import matplotlib.pyplot as plt
from sklearn.metrics import r2_score
from sklearn.metrics import mean_squared_error
from sklearn.model_selection import KFold
from sklearn.model_selection import train_test_split
from sklearn.neural_network import MLPRegressor
from sklearn import preprocessing

ds = pd.read_csv('dataset/infected.csv')

ds_death = pd.read_csv('dataset/Deaths_us.csv')

#Data ca nhiễm
x = ds['num_of_date']
y = ds['num_of_patients']
x_test_patient = ds['num_of_date_test'][:12]
y_test_patient = ds['num_of_patients_test'][:12]
x_prediction =[[95],[96],[97],[98],[99],[100],[101]]
yy=np.log10(y)
#data số người chết
x_death = ds_death['num_of_date']
y_death = ds_death['Deaths']
x_test_deaths = ds_death['num_of_date_test'][:8]
y_test_deaths = ds_death['Deaths_test'][:8]
x_prediction2 =[[60],[61],[62],[63],[64],[65],[67]]
y_log_D = np.log10(y_death)


#Sử dụng mạng nơ ron với data số ca nhiễm
scores_1 = []
scores_2 = []
scores_3 = []
scores_4 = []
scores_5 = []

MLP_Regressor_1 = MLPRegressor(hidden_layer_sizes=(4), activation='tanh', solver='lbfgs' ,learning_rate_init=0.01, max_iter=1000,random_state=120, validation_fraction=0.1)
MLP_Regressor_2 = MLPRegressor(hidden_layer_sizes=(5), activation='tanh', solver='lbfgs' ,learning_rate_init=0.01, max_iter=1000,random_state=120, validation_fraction=0.1)
MLP_Regressor_3 = MLPRegressor(hidden_layer_sizes=(1), activation='tanh', solver='lbfgs' ,learning_rate_init=0.3, max_iter=1000,random_state=120, validation_fraction=0.2)
MLP_Regressor_4 = MLPRegressor(hidden_layer_sizes=(5), activation='relu', solver='lbfgs' ,learning_rate_init=0.01, max_iter=1000,random_state=1, validation_fraction=0.1)
MLP_Regressor_5 = MLPRegressor(hidden_layer_sizes=(5), activation='tanh', solver='sgd' ,learning_rate_init=0.01, max_iter=1000,random_state=1, validation_fraction=0.1)

cv = KFold(n_splits=10, random_state=1, shuffle=True)
for train_index, test_index in cv.split(x):
    X_train, X_test, y_train, y_test ,yy_train, yy_test= x[train_index], x[test_index], y[train_index], y[test_index], yy[train_index], yy[test_index]
    #
    MLP_Regressor_1.fit(X_train.values.reshape(-1,1), yy_train)
    scores_1.append(MLP_Regressor_1.score(X_test.values.reshape(-1,1), yy_test))
    #
    MLP_Regressor_2.fit(X_train.values.reshape(-1,1), yy_train)
    scores_2.append(MLP_Regressor_2.score(X_test.values.reshape(-1,1), yy_test))
    #
    MLP_Regressor_3.fit(X_train.values.reshape(-1,1), yy_train)
    scores_3.append(MLP_Regressor_3.score(X_test.values.reshape(-1,1), yy_test))
     #
    MLP_Regressor_4.fit(X_train.values.reshape(-1,1), yy_train)
    scores_4.append(MLP_Regressor_4.score(X_test.values.reshape(-1,1), yy_test))
    #
    MLP_Regressor_5.fit(X_train.values.reshape(-1,1), yy_train)
    scores_5.append(MLP_Regressor_5.score(X_test.values.reshape(-1,1), yy_test))

print("Giá trị trung bình MLP_Regressor_1:",sum(scores_1)/10,
      "\nGiá trị trung bình MLP_Regressor_2:",sum(scores_2)/10,
      "\nGiá trị trung bình MLP_Regressor_3:",sum(scores_3)/10
      ,"\nGiá trị trung bình MLP_Regressor_4:",sum(scores_4)/10,
      "\nGiá trị trung bình MLP_Regressor_5:",sum(scores_5)/10) 


MLP_Regressor = MLPRegressor(hidden_layer_sizes=(5), activation='tanh', solver='lbfgs' ,learning_rate_init=0.01, max_iter=1000,random_state=120, validation_fraction=0.1)
MLP_Regressor.fit(x.values.reshape(-1,1), yy)
y_test_patient_log=np.log10(y_test_patient)
evaluation_3 =MLP_Regressor.predict(x_test_patient.values.reshape(-1,1))
score=MLP_Regressor.score(x_test_patient.values.reshape(-1,1), y_test_patient_log)   
print("Độ chính xác mô hình :",score)   

print('Dự đoán theo ngày:')
for predict in x_prediction:
  print('Ngày',predict,'=',int(10**MLP_Regressor.predict([predict])))
  
predicted3 =10**MLP_Regressor.predict(x.values.reshape(-1,1))
predicted4=MLP_Regressor.predict(x.values.reshape(-1,1))
plt.plot(x, y, 'o-',label='dataset')
plt.plot(x, predicted3,c='#ff0000',label='Mô hình dự đoán')
plt.legend()
plt.title('Mô hình mạng nơ ron hồi quy (Độ chính xác 99,85%)')
plt.xlabel('Ngày')
plt.ylabel('Ca nhiễm ')
plt.show()




#Sử dụng mạng nơ ron với data số người chết

scores_1 = []
scores_2 = []
scores_3 = []
scores_4 = []
scores_5 = []


MLP_Regressor_1 = MLPRegressor(hidden_layer_sizes=(4), activation='tanh', solver='lbfgs' ,learning_rate_init=0.01, max_iter=1000,random_state=93, validation_fraction=0.1)
MLP_Regressor_2 = MLPRegressor(hidden_layer_sizes=(5), activation='tanh', solver='lbfgs' ,learning_rate_init=0.01, max_iter=1000,random_state=93, validation_fraction=0.1)
MLP_Regressor_3 = MLPRegressor(hidden_layer_sizes=(6), activation='tanh', solver='lbfgs' ,learning_rate_init=0.01, max_iter=1000,random_state=93, validation_fraction=0.1)
MLP_Regressor_4 = MLPRegressor(hidden_layer_sizes=(4), activation='relu', solver='lbfgs' ,learning_rate_init=0.01, max_iter=1000,random_state=93, validation_fraction=0.1)
MLP_Regressor_5 = MLPRegressor(hidden_layer_sizes=(4), activation='tanh', solver='sgd' ,learning_rate_init=0.01, max_iter=1000,random_state=93, validation_fraction=0.1)

cv = KFold(n_splits=8, random_state=1, shuffle=True)
for train_index, test_index in cv.split(x_death):
    X_train_D, X_test_D, y_log_train_D, y_log_test_D = x_death[train_index], x_death[test_index], y_log_D[train_index], y_log_D[test_index]
    
    MLP_Regressor_1.fit(X_train_D.values.reshape(-1,1), y_log_train_D)
    scores_1.append(MLP_Regressor_1.score(X_test_D.values.reshape(-1,1), y_log_test_D))
    
    MLP_Regressor_2.fit(X_train_D.values.reshape(-1,1), y_log_train_D)
    scores_2.append(MLP_Regressor_2.score(X_test_D.values.reshape(-1,1), y_log_test_D))
    
    MLP_Regressor_3.fit(X_train_D.values.reshape(-1,1), y_log_train_D)
    scores_3.append(MLP_Regressor_3.score(X_test_D.values.reshape(-1,1), y_log_test_D))
    
    MLP_Regressor_4.fit(X_train_D.values.reshape(-1,1), y_log_train_D)
    scores_4.append(MLP_Regressor_4.score(X_test_D.values.reshape(-1,1), y_log_test_D))
    
    MLP_Regressor_5.fit(X_train_D.values.reshape(-1,1), y_log_train_D)
    scores_5.append(MLP_Regressor_5.score(X_test_D.values.reshape(-1,1), y_log_test_D))

print(  "Điểm trung bình MLP_Regressor_1:",sum(scores_1)/8,
      "\nĐiểm trung bình MLP_Regressor_2:",sum(scores_2)/8,
      "\nĐiểm trung bình MLP_Regressor_3:",sum(scores_3)/8,
      "\nĐiểm trung bình MLP_Regressor_4:",sum(scores_4)/8,
      "\nĐiểm trung bình MLP_Regressor_5:",sum(scores_5)/8)   


MLP_Regressor_D = MLPRegressor(hidden_layer_sizes=(5), activation='tanh', solver='lbfgs' ,learning_rate_init=0.01,
                               max_iter=1000,random_state=93, validation_fraction=0.1)
MLP_Regressor_D.fit(x_death.values.reshape(-1,1), y_log_D)
y_test_deaths_log=np.log10(y_test_deaths)
evaluation_6 =MLP_Regressor_D.predict(x_test_deaths.values.reshape(-1,1))
score=MLP_Regressor_D.score(x_test_deaths.values.reshape(-1,1), y_test_deaths_log)   
print("Độ chính xác mô hình :",score)

print('Dự đoán theo ngày: ')
for predict in x_prediction2:
  print('day',predict,'=',int(10**MLP_Regressor_D.predict([predict])))

predicted6 = MLP_Regressor_D.predict(x_death.values.reshape(-1,1))
plt.plot(x_death, y_death, 'o-',label='dataset')
plt.plot(x_death, 10**predicted6,c='#ff0000',label='Mô hình dự đoán')
plt.legend()
plt.title('Mô hình mạng nơ ron hồi quy (Độ chính xác 99,98%)')
plt.xlabel('Số ngày')
plt.ylabel('Số người chết')
plt.show()