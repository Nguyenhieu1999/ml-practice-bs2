from keras.models import Sequential
from keras.layers import Conv2D
from keras.layers import MaxPooling2D
from keras.layers import Flatten
from keras.layers import Dense

# Khởi tạo CNN
classifier = Sequential()


classifier.add(Conv2D(32, (3, 3), input_shape = (32,32,3), activation = 'relu'))


classifier.add(MaxPooling2D(pool_size = (2, 2)))


classifier.add(Conv2D(32, (3, 3), activation = 'relu'))
classifier.add(MaxPooling2D(pool_size = (2, 2)))


classifier.add(Flatten())

classifier.add(Dense(units = 128, activation = 'relu'))
classifier.add(Dense(units = 26, activation = 'softmax'))


classifier.compile(optimizer = 'adam', loss = 'categorical_crossentropy', metrics = ['accuracy'])

#Lắp CNN vào hình ảnh

from keras.preprocessing.image import ImageDataGenerator

train_datagen = ImageDataGenerator(rescale = 1./255,
                                   shear_range = 0.2,
                                   zoom_range = 0.2,
                                   horizontal_flip = True)

test_datagen = ImageDataGenerator(rescale = 1./255)

training_set = train_datagen.flow_from_directory('English Alphabet Dataset/Training',
                                                 target_size = (32, 32),
                                                 batch_size = 32,
                                                 class_mode = 'categorical')

test_set = test_datagen.flow_from_directory('English Alphabet Dataset/Testing',
                                            target_size = (32, 32),
                                            batch_size = 32,
                                            class_mode = 'categorical')

classifier.fit_generator(training_set,
                         steps_per_epoch = 3000,
                         epochs = 1,
                         validation_data = test_set,
                         validation_steps = 2000)


#Tạo dự đoán

import numpy as np
from keras.preprocessing import image
test_image = image.load_img('English Alphabet Dataset/28.png', target_size = (32,32))
test_image = image.img_to_array(test_image)
test_image = np.expand_dims(test_image, axis = 0)
result = classifier.predict(test_image)



if result[0][0] == 1:
    print('a')
elif result[0][1] == 1:
    print('b')
elif result[0][2] == 1:
    print('c')
elif result[0][3] == 1:
    print('d')
elif result[0][4] == 1:
    print('e')
elif result[0][5] == 1:
    print('f')
elif result[0][6] == 1:
    print('g')
elif result[0][7] == 1:
    print('h')
elif result[0][8] == 1:
    print('i')
elif result[0][9] == 1:
    print('j')
elif result[0][10] == 1:
    print('k')
elif result[0][11] == 1:
    print('l')
elif result[0][12] == 1:
    print('m')
elif result[0][13] == 1:
    print('n')
elif result[0][14] == 1:
    print('o')
elif result[0][15] == 1:
    print('p')
elif result[0][16] == 1:
    print('q')
elif result[0][17] == 1:
    print('r')
elif result[0][18] == 1:
    print('s')
elif result[0][19] == 1:
    print('t')
elif result[0][20] == 1:
    print('u')
elif result[0][21] == 1:
    print('v')
elif result[0][22] == 1:
    print('w')
elif result[0][23] == 1:
    print('x')
elif result[0][24] == 1:
    print('y')
elif result[0][25] == 1:
    print('z')